// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload")
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  const encodedPayload = JSON.parse(payloadCookie.value);
  // console.log("encodedPayload:::", encodedPayload);

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(encodedPayload)

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload)

  // Print the payload
  console.log(payload);

  let perms = payload.user.perms

  // Check if "events.add_conference" is in the permissions.
  if (perms.includes("events.add_conference")){
    const conLogged = document.getElementById('con-logged')
    conLogged.classList.remove("d-none")
  }
  // If it is, remove 'd-none' from the link
  if (perms.includes("events.add_location")){
    const loggedIn = document.getElementById('logged-in')
    loggedIn.classList.remove("d-none")
  }

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link

}